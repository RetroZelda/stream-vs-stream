﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using FSMLibraryNS;

public class SplashScreenState : BaseState
{
    public override void OnEnter()
    {
        base.OnEnter();
        BlackFader.Fade(false, 2.0f);
    }
}
