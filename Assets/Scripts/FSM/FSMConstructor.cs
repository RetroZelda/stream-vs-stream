﻿using UnityEngine;
using System.Collections;
using FSMLibraryNS;
using System;

public class FSMConstructor : IFSMConstructor
{
    public static string _Chapter = "GameConstructor";
    public static int _ChapterHash = _Chapter.GetHashCode();

    public string Chapter { get { return _Chapter; } }
    public int ChapterHash { get { return _ChapterHash; } }

    public void Build()
    {
        FSMLibrary.Chapter(_ChapterHash).AddState(typeof(SplashScreenState));
        FSMLibrary.Chapter(_ChapterHash).AddState(typeof(MainMenuState));
        FSMLibrary.Chapter(_ChapterHash).AddState(typeof(JoinGameState));
        FSMLibrary.Chapter(_ChapterHash).AddState(typeof(PauseState));
        FSMLibrary.Chapter(_ChapterHash).AddState(typeof(GameState));

        FSMLibrary.Chapter(_ChapterHash).SetDefaultState(typeof(SplashScreenState));
    }

    public void Destroy()
    {
        FSMLibrary.Chapter(_ChapterHash).RemoveState(typeof(GameState));
        FSMLibrary.Chapter(_ChapterHash).RemoveState(typeof(PauseState));
        FSMLibrary.Chapter(_ChapterHash).RemoveState(typeof(JoinGameState));
        FSMLibrary.Chapter(_ChapterHash).RemoveState(typeof(MainMenuState));
        FSMLibrary.Chapter(_ChapterHash).RemoveState(typeof(SplashScreenState));
    }
}
