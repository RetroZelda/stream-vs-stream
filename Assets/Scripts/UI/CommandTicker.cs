﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using DG.Tweening;

public class CommandTicker : MonoBehaviour
{
    [SerializeField]
    private Image _Background;

    [SerializeField]
    private Text _Header;

    [SerializeField]
    private Text _CommandDisplay;

    [SerializeField]
    private ScrollRect _ScrollRect;

    public StreamInput InputToWatch
    {
        get; private set;
    }

    public void Init(StreamInput playerInput)
    {
        InputToWatch = playerInput;
        _Header.text = string.Format("Player: {0}", playerInput.name);

        // TODO: Get Color from player
        _Background.color = Random.ColorHSV();

        ClearDisplay();

        // sub to events
        InputToWatch.OnVoteComplete += HandleVoteComplete;
        InputToWatch.PlayerInfo.OnGameCommand += HandleNewCommand;

    }

    public void Destroy()
    {
        // unsub from events
        InputToWatch.OnVoteComplete -= HandleVoteComplete;
        InputToWatch.PlayerInfo.OnGameCommand -= HandleNewCommand;
    }

    private void HandleNewCommand(string szUser, string szCommand)
    {
        _CommandDisplay.text += string.Format("\n{0} - {1}", szUser, szCommand);
        ScrollToBottom();
    }

    private void HandleVoteComplete(string szMessage)
    {
        _CommandDisplay.text += string.Format("\n{0}", szMessage);
    }

    public void ClearDisplay()
    {
        _CommandDisplay.text = "";
        ScrollToTop();
    }

    public void ScrollToTop()
    {
        DOTween.To(() => _ScrollRect.normalizedPosition, x => _ScrollRect.normalizedPosition = x, new Vector2(0.0f, 1.0f), 0.25f);
    }

    public void ScrollToBottom()
    {
        DOTween.To(() => _ScrollRect.normalizedPosition, x => _ScrollRect.normalizedPosition = x, new Vector2(0.0f, 0.0f), 0.25f);
    }
}
