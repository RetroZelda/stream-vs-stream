﻿using UnityEngine;
using System.Collections.Generic;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _CommandTickerPrefab;

    private RectTransform _RightElement;
    private RectTransform _BottomElement;

    private List<CommandTicker> _Tickers;

    public static UIManager Instance { get; private set; }

    // Use this for initialization
    void Start ()
    {
        if (Instance != null)
            throw new System.AccessViolationException("There is already an instantiated UIManager running.");
        
        _RightElement = transform.FindChild("Panel_Right") as RectTransform;
        _BottomElement = transform.FindChild("Panel_Bottom") as RectTransform;
        _Tickers = new List<CommandTicker>();
        Instance = this;
    }

    public static void AddPlayerTicker(StreamInput playerInput)
    {
        GameObject newTickerObj = Instantiate<GameObject>(Instance._CommandTickerPrefab);
        newTickerObj.transform.SetParent(Instance._RightElement);
        newTickerObj.transform.localScale = Vector3.one;

        CommandTicker newTicker = newTickerObj.GetComponent<CommandTicker>();
        newTicker.Init(playerInput);

        Instance._Tickers.Add(newTicker);
    }

    public static void RemovePlayerTicker(StreamInput playerInput)
    {
        for(int nTickerIndex = 0; nTickerIndex < 0; ++nTickerIndex)
        {
            if(Instance._Tickers[nTickerIndex].InputToWatch == playerInput)
            {
                Instance._Tickers.RemoveAt(nTickerIndex);
                return;
            }
        }
    }
}
