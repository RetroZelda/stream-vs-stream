﻿using UnityEngine;
using System.Collections;

public interface IChatBot
{
    bool IsConnected { get; }
    OnChatMessageEvent OnChatMessage { get; set; }

    void SendMessage(string szUser, string szMessage);
    void PurgeMessage(string szUser, int nTimeout);
    void Connect();
    void Disconnect();
    void Write(string szText, params object[] arg);
    bool Read(out string szRead);
}
