﻿using UnityEngine;
using System.Collections;
using System;

public class Bomb : AttachableObject
{
    [SerializeField]
    private ParticleSystem _blastEffect;

    [SerializeField]
    private int _blastSize = 3;

    [SerializeField]
    private float _fuseTime = 1.0f;

    [SerializeField]
    private float _blastTime = 2.0f;

    public Player BombOwner { get; private set; }

    private float _curTime;
    private bool _isDeployed;
    private bool _isBlasted;
    
    public void SetFuse(Player owner, float fFuseTime, float fBlastTime, int nBlastSize)
    {
        if (_isDeployed)
            return;

        _fuseTime = fFuseTime;
        _blastTime = fBlastTime;
        _blastSize = nBlastSize;

        SetFuse(owner);
    }

    public void SetFuse(Player owner)
    {
        if (_isDeployed)
            return;
        BombOwner = owner;
        _curTime = _fuseTime;
        _isDeployed = true;
    }

	// Update is called once per frame
	void Update ()
    {
	    if(_isDeployed)
        {
            _curTime -= Time.deltaTime;
            if (_isBlasted)
            {
                // TODO: handle blast effect
            }
            else
            {
                // TODO: Handle fuse effect
                if(_curTime < 0.0f)
                {
                    Detonate();
                }
            }
        }
	}

    public void Detonate()
    {
        _isBlasted = true;
        float fRotY = 0.0f;
        int nBlocksToBlast = 0;
        foreach (BlockDirection dir in Enum.GetValues(typeof(BlockDirection)))
        {
            fRotY = 0.0f + ((float)dir * 90.0f); // TODO: Use the 0.0f to offset the order
            ParticleSystem blast = Instantiate<GameObject>(_blastEffect.gameObject).GetComponent<ParticleSystem>();
            blast.transform.SetParent(transform);
            blast.transform.localPosition = Vector3.zero;
            blast.transform.localRotation = Quaternion.Euler(0.0f, fRotY, 0.0f);
            blast.transform.localScale = Vector3.one;

            nBlocksToBlast = OwningBlock.BlockTraverse((int)_blastSize, dir, BlastCheck);

            blast.startLifetime = (nBlocksToBlast) / 25.0f;
            blast.loop = false;
            blast.Play();
        }
        _curTime = _blastTime;
    }

    private bool BlastCheck(Block block)
    {
        bool bReturn = false;

        // stop at walls
        if (block.AttachedObject != null && block.AttachedObject.ObjType == ObjectType.Wall)
        {
            Wall wall = block.AttachedObject as Wall;
            wall.AdjustDamage(1); // do 1 damage to wall.  if destructible itll get marked for destruction
            bReturn = true;
        }

        // detonate other bombs and continue
        if (block.AttachedObject != null && block.AttachedObject.ObjType == ObjectType.Bomb && block.AttachedObject != this)
        {
            Bomb bomb = block.AttachedObject as Bomb;
            if(!bomb._isBlasted)
                bomb.Detonate();
        }

        // kill enemy players and continue
        foreach(Player p in block.Occupants)
        {
            if(p != BombOwner)
                p.TakeDamage(10);
        }

        return bReturn;
    }

    public override bool ShouldDestroy()
    {
        return _isDeployed && _isBlasted && _curTime <= 0.0f;
    }
}
