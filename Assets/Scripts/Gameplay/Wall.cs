﻿using UnityEngine;
using System.Collections;
using System;

public class Wall : AttachableObject
{
    [SerializeField]
    private int _maxHealth = 0; // 0 = indestructible

    private int _curHealth;

    // Use this for initialization
    void Start ()
    {
        _curHealth = _maxHealth;
    }
	
    // stupid function, i know.  sorry....
    public void AdjustDamage(int nDamageAmmount)
    {
        _curHealth -= nDamageAmmount;
    }

    public override bool ShouldDestroy()
    {
        return _maxHealth > 0 && _curHealth <= 0;
    }
}
