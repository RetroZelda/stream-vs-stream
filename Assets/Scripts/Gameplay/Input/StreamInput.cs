﻿using UnityEngine;
using System;
using System.Collections;

public delegate void VoteCompleteMessage(string szMessage);
public class StreamInput : BaseInput
{
	private enum VoteType
	{
		up,
		down,
		left,
		right,
		bomb,
        wait,
        TOTAL_COMMANDS
	}

    private int[] Votes { get; set; }

    private PlayerSpawnInformation _PlayerInfo;
    public PlayerSpawnInformation PlayerInfo { get { return _PlayerInfo; } private set { _PlayerInfo = value; } }

    public VoteCompleteMessage OnVoteComplete;

    public void Init(PlayerSpawnInformation playerInfo)
    {
        PlayerInfo = playerInfo;
        PlayerInfo.OnGameCommand += HandleNewCommand;
        name = _PlayerInfo.TeamID;

        Votes = new int[(int)VoteType.TOTAL_COMMANDS];

        UIManager.AddPlayerTicker(this);
    }

    public void Shutdown()
    {
        PlayerInfo.OnGameCommand -= HandleNewCommand;
        UIManager.RemovePlayerTicker(this);
    }

    public void Vote(string vote)
    {
        VoteType TheVote = (VoteType)Enum.Parse(typeof(VoteType), vote);
        ++Votes[(int)TheVote];
    }


    // Update is called once per frame
    void Update ()
    {
		if(m_Player.isDoneMoving())
			SendInput();		
	}

    void OnDestroy()
    {
        Shutdown();
    }

    private void HandleNewCommand(string szUser, string szCommand)
    {
        Debug.Log(string.Format("{0} - Recieved vote for Command: {1}.", name, szCommand));
        Vote(szCommand);
    }

	private void SendInput()
	{
		//Find Winner
		int winner = 0;
		int index = -1;
		for(int i = 0; i < 5; ++i) 
		{
			if(Votes[i] > winner)
			{
				winner = Votes[i];
				index = i;
			}
		}

        // wipe voites
        ClearVotes();

        if (index == -1)
			return; // No Inputs

        Debug.Log(string.Format("{0} - {1} won command vote!", name, ((VoteType)index).ToString()));
        if(OnVoteComplete != null)
        {
            OnVoteComplete.Invoke(string.Format("{0} won command vote!", ((VoteType)index).ToString()));
        }

        if (index == (int)VoteType.up) m_Player.MoveUp();
		else if(index == (int)VoteType.down) m_Player.MoveDown();
		else if(index == (int)VoteType.left) m_Player.MoveLeft();
		else if(index == (int)VoteType.right) m_Player.MoveRight();
        else if (index == (int)VoteType.bomb) m_Player.PlaceBomb();

    }

	private void ClearVotes () 
	{
		for(int i = 0; i < 5; ++i) 
		{
			Votes[i] = 0;
		}
	}		
}
