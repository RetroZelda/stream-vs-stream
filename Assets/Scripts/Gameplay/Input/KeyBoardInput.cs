﻿using UnityEngine;
using System.Collections;

public class KeyBoardInput : BaseInput {

	// Use this for initialization
	void Start () {
		base.Start();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown("a")){m_Player.MoveLeft();}
		if(Input.GetKeyDown("s")){m_Player.MoveDown();}
		if(Input.GetKeyDown("d")){m_Player.MoveRight(); }
        if (Input.GetKeyDown("w")) { m_Player.MoveUp(); }
        if (Input.GetKeyDown(KeyCode.Space)) { m_Player.PlaceBomb(); }
    }
}
