﻿using UnityEngine;
using System.Collections;

public enum ObjectType
{
    Bomb,
    SpawnPoint,
    Wall,
}
public abstract class AttachableObject : MonoBehaviour
{
    [SerializeField]
    private ObjectType _objectType;

    public ObjectType ObjType { get { return _objectType; } }

    public Block OwningBlock { get; set; }

    public abstract bool ShouldDestroy();
}
