﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    [SerializeField]
    private float _MoveTimer = 1.0f;

    [SerializeField]
    private int _MaxHealth = 10;

    private Animator _animator;
    private Block _myBlock;
    private Grid _grid;

    public int Health { get; private set; }
    public bool IsDead { get { return Health <= 0; } }

    //accessors
    public Block MyBlock
    {
        get { return _myBlock; }

        set
        {
            if(_myBlock != null)
                _myBlock.Occupants.Remove(this);
            if(value != null)
                value.Occupants.Add(this);
            _myBlock = value;
        }
    }
    
    void Start()
    {
        FindMyBlock();
        Health = _MaxHealth;
    }

    void Update()
    {
        if (!isDoneMoving())
        {
            UpdateToBlock();
        }
    }

    private void FindGrid()
    {
        _grid = Component.FindObjectOfType<Grid>();
    }

    private void FindMyBlock()
    {
        if (_grid == null)
            FindGrid();
        MyBlock = _grid.FindNearestBlock(transform.position);
        transform.position = MyBlock.AttachPoint;
    }

    private void UpdateToBlock()
    {
        transform.position = Vector3.MoveTowards(transform.position, MyBlock.AttachPoint, _MoveTimer * Time.deltaTime * _grid.BlockWidth);
    }
    
    public void PlaceBomb()
    {
        if (MyBlock.AttachedObject != null || !isDoneMoving())
            return;
        GameObject bombPrefab = Global.PrefabLib.Obtain("Bomb");
        Bomb bomb = Instantiate<GameObject>(bombPrefab).GetComponent<Bomb>();
        MyBlock.AttachObject(bomb);

        bomb.SetFuse(this);
    }

    public void MoveLeft()
    {
        if (isDoneMoving() && MyBlock.LeftBlock.AttachedObject == null || (MyBlock.LeftBlock.AttachedObject != null && MyBlock.LeftBlock.AttachedObject.ObjType != ObjectType.Wall))
            MyBlock = MyBlock.LeftBlock;
    }
    public void MoveRight()
    {
        if (isDoneMoving() && MyBlock.RightBlock.AttachedObject == null || (MyBlock.RightBlock.AttachedObject != null && MyBlock.RightBlock.AttachedObject.ObjType != ObjectType.Wall))
            MyBlock = MyBlock.RightBlock;
    }
    public void MoveUp()
    {
        if (isDoneMoving() && MyBlock.UpBlock.AttachedObject == null || (MyBlock.UpBlock.AttachedObject != null && MyBlock.UpBlock.AttachedObject.ObjType != ObjectType.Wall))
            MyBlock = MyBlock.UpBlock;
    }
    public void MoveDown()
    {
        if (isDoneMoving() && MyBlock.DownBlock.AttachedObject == null || (MyBlock.DownBlock.AttachedObject != null && MyBlock.DownBlock.AttachedObject.ObjType != ObjectType.Wall))
            MyBlock = MyBlock.DownBlock;
    }

    public void TakeDamage(int nDmg)
    {
        Health -= nDmg;

        // TODO: Handle death.
        if(IsDead)
        {
            Debug.LogWarning(string.Format("Player {0} has died.", name));
        }
    }

    public bool isDoneMoving()
    {
        return Vector3.Distance(transform.position, MyBlock.AttachPoint) < 0.001f;
    }
}
