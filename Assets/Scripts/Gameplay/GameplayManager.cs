﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(GameCommander))]
public class GameplayManager : MonoBehaviour
{
    private GameObject _PlayerPrefab;
    private GameObject _GridPrefab;
    private GameObject _SpawnpointPrefab;
    private GameObject _DestructibleWallPrefab;
    private GameObject _IndestructibleWallPrefab;

    private GameCommander _GameCommander;
    private Grid _Grid;

    private SpawnPoint[] _SpawnPoints;
    private Player[] _ActivePlayers;

    void Awake()
    {
        GameCommander.INITONSTART = false;
    }

    // Use this for initialization
    void Start()
    {
        _PlayerPrefab = Global.PrefabLib.Obtain("Player");
        _GridPrefab = Global.PrefabLib.Obtain("Grid");
        _SpawnpointPrefab = Global.PrefabLib.Obtain("SpawnPoint");
        _DestructibleWallPrefab = Global.PrefabLib.Obtain("DestructibleWall");
        _IndestructibleWallPrefab = Global.PrefabLib.Obtain("IndestructibleWall");

        _GameCommander = GetComponent<GameCommander>();
        _Grid = Instantiate<GameObject>(_GridPrefab).GetComponent<Grid>();

        // NOTE: Might eventually move this logic to FSM
        // Setup what is needed for a game
        _GameCommander.Initialize();
        GenerateLevel();
    }

    public void GenerateLevel()
    {
        PlayerSpawnInformation[] spawnInfo = _GameCommander.SpawnInfo;

        _Grid.BuildGrid(15, 13);
        PlaceWalls();
        PlaceSpawnPoints(spawnInfo.Length);
        SpawnPlayers(spawnInfo);
    }

    public void PlaceWalls()
    {
        Wall newWall = null;
        for(int nRow = 0; nRow < _Grid.NumberOfRows; ++nRow)
        {
            for (int nCol = 0; nCol < _Grid.NumberOfColums; ++nCol)
            {
                // place solid walls on outside perimeter
                if (nRow == 0 || nCol == 0 || nRow == _Grid.NumberOfRows - 1 || nCol == _Grid.NumberOfColums - 1)
                {
                    newWall = Instantiate<GameObject>(_IndestructibleWallPrefab).GetComponent<Wall>();
                }
                // place solid walls on every other grid spot
                else if ((nRow & 1) == 0 && (nCol & 1) == 0)
                {
                    newWall = Instantiate<GameObject>(_IndestructibleWallPrefab).GetComponent<Wall>();
                }
                // fill other spots with destructible walls
                else
                {
                    newWall = Instantiate<GameObject>(_DestructibleWallPrefab).GetComponent<Wall>();
                }

                // the walls need to match block scale
                newWall.transform.localScale = new Vector3(_Grid.BlockWidth, _Grid.BlockWidth, _Grid.BlockWidth);

                // attach
                Block block = _Grid.GetBlock(nRow, nCol);
                block.AttachObject(newWall, true);
            }
        }
    }

    public void PlaceSpawnPoints(int nNumSpawns)
    {
        // we want to place all the spawns evenly around the grid
        _SpawnPoints = new SpawnPoint[nNumSpawns];

        // start with center tile
        float fW = ((float)_Grid.NumberOfRows) * 0.5f;
        float fH = ((float)_Grid.NumberOfColums) * 0.5f;
        Vector2 v2Center = new Vector2(fW, fH);
        Block centerBlock = _Grid.GetBlock((int)v2Center.x, (int)v2Center.y);
        centerBlock.SetColor(Color.black);

        float fCurDegree = 90.0f;
        float fDegreeStep = 360.0f / (float)nNumSpawns;
        for(int nSpawnIndex = 0; nSpawnIndex < nNumSpawns; ++nSpawnIndex)
        {
            // get the block
            int nBlockX = (int)(v2Center.x + (fW * Mathf.Cos(Mathf.Deg2Rad * fCurDegree)));
            int nBlockY = (int)(v2Center.y + (fH * Mathf.Sin(Mathf.Deg2Rad * fCurDegree)));
            fCurDegree += fDegreeStep;

            // ensure its not on the edge
            while (nBlockX <= 0) nBlockX++;
            while (nBlockY <= 0) nBlockY++;
            while (nBlockX >= _Grid.NumberOfRows - 1) nBlockX--;
            while (nBlockY >= _Grid.NumberOfColums - 1) nBlockY--;

            // ensure its not on an even block


            // place a spawn
            Block block = _Grid.GetBlock(nBlockX, nBlockY);
            SpawnPoint sp = Instantiate<GameObject>(_SpawnpointPrefab).GetComponent<SpawnPoint>();
            sp.transform.LookAt(centerBlock.AttachPoint, Vector3.up);
            _SpawnPoints[nSpawnIndex] = sp;
            block.AttachObject(sp, true);

            // remove anything attached to adjacent blocks
            foreach(Block b in block.AdjacentBlocks)
            {
                if (b.RowIndex == 0 || b.ColIndex == 0 ||
                    b.RowIndex == _Grid.NumberOfRows - 1 ||
                    b.ColIndex == _Grid.NumberOfColums - 1)
                        continue;

                b.AttachObject(null, true);
            }
        }
    }
	
    public void SpawnPlayers(PlayerSpawnInformation[] spawnInfo)
    {
        Vector3 v3SpawnPos = Vector3.zero;

        int nNewPlayerIndex = 0;
        _ActivePlayers = new Player[spawnInfo.Length];
        foreach(PlayerSpawnInformation playerSpawn in spawnInfo)
        {
            GameObject newPlayerObj = Instantiate<GameObject>(_PlayerPrefab);
            Player newPlayer = newPlayerObj.GetComponent<Player>();
            BaseInput input = newPlayer.GetComponent<BaseInput>();

            // remove any potential input that isnt stream
            if(input != null)
            {
                if(!(input is StreamInput))
                {
                    Destroy(input);
                    input = newPlayerObj.AddComponent<StreamInput>();
                }
            }

            // setup the input
            StreamInput streamInput = input as StreamInput;
            streamInput.Init(playerSpawn);

            // TODO: Spawn the Player at a spawn block
            newPlayerObj.transform.position = _SpawnPoints[nNewPlayerIndex].transform.position;

            _ActivePlayers[nNewPlayerIndex++] = newPlayer;
        }
    }
}
