﻿using UnityEngine;
using System.Collections;

public class Grid : MonoBehaviour {

    [SerializeField]
    private Color[] _gridColors;

	private Block[,] Blocks;
    private GameObject _blockPrefab;

    public float BlockWidth = 1.0f; // X and Z size
    public float BlockHeight = 1.0f; // Y size
    
    public int NumberOfRows = 10;
	public int NumberOfColums = 10;
    
	void Awake () 
	{
        _blockPrefab = Global.PrefabLib.Obtain("Block");
    }
	
	// Update is called once per frame
	void Update () 
	{
	
	}
		
	public Block GetBlock(int row, int col)
	{
		if(row < NumberOfRows && row > -1 && col < NumberOfColums && col > -1)
			return Blocks[row,col];
		else
			return null;
	}

	public void BuildGrid(int rowtotal, int coltotal)
	{
		NumberOfRows = rowtotal;
		NumberOfColums = coltotal;
		Blocks = new Block[rowtotal, coltotal];

        int nCurColorIndex = 0;
        int nStartColorIndex = 0;
        for (int row = 0; row < rowtotal; ++row)
        {
            nCurColorIndex = nStartColorIndex++;
            for (int col = 0; col < coltotal; ++col)
            {
                Block block = CreateBlock(row, col);
                block.SetColor(_gridColors[nCurColorIndex++]);

                if (nCurColorIndex >= _gridColors.Length)
                    nCurColorIndex = 0;
            }
            if (nStartColorIndex >= _gridColors.Length)
                nStartColorIndex = 0;
        }
        Refresh();
    }

	private Block CreateBlock(int row, int col)
	{
		Vector3 TempPosition = new Vector3(0,0,0);
		Vector3 TempScale = new Vector3(0,0,0);

		GameObject OpenBlock = GameObject.Instantiate(_blockPrefab);
		OpenBlock.SetActive(true);

		TempPosition.x = BlockWidth * row + BlockWidth/2;
		TempPosition.z = BlockWidth * col + BlockWidth/2;

		TempScale.x = BlockWidth;
		TempScale.y = BlockHeight;
		TempScale.z = BlockWidth;

        OpenBlock.transform.SetParent(transform);
        OpenBlock.transform.position = TempPosition;
		OpenBlock.transform.localScale = TempScale;


		Blocks[row,col] = OpenBlock.GetComponent<Block>();
		Blocks[row,col].SetUp(this, row, col);
        return Blocks[row, col];
    }

	//Consider Moving this into a camera class. No need for right now. 
	private void CenterCameraPosition()
	{
		Vector3 TempPosition = FindObjectOfType<Camera>().transform.position;

		TempPosition.x = BlockWidth * NumberOfRows/2;
		TempPosition.z = BlockWidth * NumberOfColums/2;

		FindObjectOfType<Camera>().transform.position = TempPosition;
	}

	public Block FindNearestBlock(Vector3 _Position)
	{
		float closest = Vector3.Distance(_Position, Blocks[0,0].transform.position);
		int tRow = 0;
		int tCol = 0;
		float temp;

		for(int row = 0; row < NumberOfRows; ++row)
			for(int col = 0; col < NumberOfColums; ++col)
			{
				temp = Vector3.Distance(_Position, Blocks[row, col].transform.position);
				if(temp < closest)
				{
					closest = temp;
					tRow = row;
					tCol = col;
				}
			}

		return Blocks[tRow,tCol];
	}

    public void Refresh()
    {
        for (int row = 0; row < NumberOfRows; ++row)
        {
            for (int col = 0; col < NumberOfColums; ++col)
            {
                Blocks[row, col].Refresh();
            }
        }

        // CenterCameraPosition();
    }
}
