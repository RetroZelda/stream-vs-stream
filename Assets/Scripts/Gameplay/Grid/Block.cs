﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum BlockDirection
{
    Up,
    Right,
    Down,
    Left,
}

[RequireComponent(typeof(MeshRenderer))]
public class Block : MonoBehaviour
{
    public delegate bool BlockTraversalDelimiter(Block block);

    private List<Player> _occupants = new List<Player>();

    public Grid Grid { get; private set; }
    public AttachableObject AttachedObject { get; private set; }

    public Block UpBlock { get; private set; }
	public Block DownBlock { get; private set; }
    public Block LeftBlock { get; private set; }
    public Block RightBlock { get; private set; }

    public int RowIndex { get; private set; }
    public int ColIndex { get; private set; }

    public List<Player> Occupants { get { return _occupants; } private set { _occupants = value; } }

    public Vector3 AttachPoint
    {
        get
        {
            Vector3 worldPos = transform.position;
            worldPos.y += Grid.BlockHeight * 0.5f;
            return worldPos;
        }
    }

    public Vector3 LocalAttachPoint
    {
        get
        {
            Vector3 localPos = Vector3.zero;
            localPos.y += Grid.BlockHeight * 0.5f;
            return localPos;
        }
    }

    public IEnumerable AdjacentBlocks
    {
        get
        {
            if (UpBlock != null)
                yield return UpBlock;
            if (DownBlock != null)
                yield return DownBlock;
            if (LeftBlock != null)
                yield return LeftBlock;
            if (RightBlock != null)
                yield return RightBlock;
        }
    }

    private MeshRenderer _MeshRenderer;

    /// <summary>
    /// Traverse the blocks in a direction
    /// </summary>
    /// <param name="nDepth">The max count to traverse</param>
    /// <param name="direction">The direction to traverse</param>
    /// <param name="delimiter">A delimination check</param>
    /// <returns>Number of blocks traversed</returns>
    public int BlockTraverse(int nDepth, BlockDirection direction, BlockTraversalDelimiter delimiter)
    {
        if (delimiter(this))
            return 0;

        if(nDepth > 0)
        {
            switch (direction)
            {
                case BlockDirection.Up:
                    if(UpBlock != null)
                        return UpBlock.BlockTraverse(nDepth - 1, direction, delimiter) + 1;
                    break;
                case BlockDirection.Down:
                    if (DownBlock != null)
                        return DownBlock.BlockTraverse(nDepth - 1, direction, delimiter) + 1;
                    break;
                case BlockDirection.Left:
                    if (LeftBlock != null)
                        return LeftBlock.BlockTraverse(nDepth - 1, direction, delimiter) + 1;
                    break;
                case BlockDirection.Right:
                    if (RightBlock != null)
                        return RightBlock.BlockTraverse(nDepth - 1, direction, delimiter) + 1;
                    break;
            }
        }
        return 0;
    }

    public void Awake()
    {
        _MeshRenderer = GetComponent<MeshRenderer>();
    }

    public void SetUp(Grid _grid, int _RowIndex, int _ColIndex)
	{
        Grid = _grid;
		RowIndex = _RowIndex;
		ColIndex = _ColIndex;
	}

    public void SetColor(Color col)
    {
        _MeshRenderer.material.SetColor("_Color", col);
    }

    public void Refresh()
	{
		UpBlock = Grid.GetBlock(RowIndex, ColIndex + 1);
		DownBlock = Grid.GetBlock(RowIndex, ColIndex - 1);
		LeftBlock = Grid.GetBlock(RowIndex - 1, ColIndex);
		RightBlock = Grid.GetBlock(RowIndex + 1, ColIndex);
	}

    public void Update()
    {
        if (AttachedObject != null)
        {
            if (AttachedObject.ShouldDestroy())
            {
                AttachedObject.OwningBlock = null;
                Destroy(AttachedObject.gameObject);
            }
        }
    }

    public bool AttachObject(AttachableObject obj, bool bForceDetach = false)
    {
        if(AttachedObject != null)
        {
            if(AttachedObject.ShouldDestroy() || bForceDetach)
            {
                Destroy(AttachedObject.gameObject);
            }
            else
            {
                return false;
            }
        }

        AttachedObject = obj;
        if(obj)
        {
            obj.OwningBlock = this;
            obj.transform.SetParent(transform);
            obj.transform.position = AttachPoint;
        }
        return true;
    }
}
