﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using System;

/// <summary>
/// Fade To Black.  NOTE: Not tested after the Storybook was removed.
/// </summary>
[RequireComponent(typeof(CanvasGroup))]
public class BlackFader : MonoBehaviour
{
    // these are for events
    public static string _Fade = "BlackFader";
    public static int FadeHash = _Fade.GetHashCode();

    private CanvasGroup Group;

    private static Queue<FadeEvent> FadeQueue = new Queue<FadeEvent>();
    private static bool InCoroutene = false;
    private static BlackFader Instance;
    void Awake()
    {
        Instance = this;
        Group = GetComponent<CanvasGroup>();
        
    }

    void OnDestroy()
    {
    }
    
    public static void Fade(bool bFadeIn, float fTime = 1.0f, Action onFadeComplete = null)
    {
        FadeQueue.Enqueue(new FadeEvent(bFadeIn, fTime, onFadeComplete));

        if (!InCoroutene)
        {
            if (Instance)
            {
                Instance.StartCoroutine(FadeQueueHandler());
            }
        }
    }

    public static WaitForSeconds WaitForFade(bool bFadeIn, float fTime = 1.0f)
    {
        Fade(bFadeIn, fTime);
        return new WaitForSeconds(fTime);
    }

    private static IEnumerator FadeQueueHandler()
    {
        Instance.Group.blocksRaycasts = true;
        InCoroutene = true;
        while (FadeQueue.Count > 0)
        {
            FadeEvent e = FadeQueue.Dequeue();
            
            if(e.FadeIn)
            {
                Instance.Group.alpha = 0.0f;
                Instance.Group.DOFade(1.0f, e.Time);
            }
            else
            {
                Instance.Group.alpha = 1.0f;
                Instance.Group.DOFade(0.0f, e.Time);
            }

            yield return new WaitForSeconds(e.Time);

            if (e.OnFadeComplete != null)
            {
                e.OnFadeComplete.Invoke();
            }
        }
        InCoroutene = false;
        Instance.Group.blocksRaycasts = false;
    }
}

public class FadeEvent
{
    public float Time { get; private set; }
    public bool FadeIn { get; private set; }
    public Action OnFadeComplete { get; private set; }
    public FadeEvent(bool fadein, float time, Action onFadeComplete)
    {
        Time = time;
        FadeIn = fadein;
        OnFadeComplete = onFadeComplete;
    }
}
