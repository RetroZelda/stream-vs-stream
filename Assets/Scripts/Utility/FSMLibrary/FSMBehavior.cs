﻿using UnityEngine;
using System.Collections;
using FSMLibraryNS;

public class FSMBehavior : MonoBehaviour
{
    // Use this for initialization
    void Awake()
    {
        Debug.Log("FSMBehavior - Awake");

        // add all initial constructors
        FSMLibrary.AddConstructor<FSMConstructor>();

        // build all constructors
        FSMLibrary.Build();

        // Start Library here because OnStart() needs to happen before OnEnable, and Start() doesnt
        FSMLibrary.StartLibrary();
    }

    void Start()
    {
        Debug.Log("FSMBehavior - Start");
    }

    void OnDestroy()
    {
        Debug.Log("FSMBehavior - OnDestroy");
        FSMLibrary.StopLibrary();
    }


    void OnEnable()
    {
        Debug.Log("FSMBehavior - OnEnable");
        FSMLibrary.ResumeLibrary();
    }

    void OnDisable()
    {
        Debug.Log("FSMBehavior - OnDisable");
        FSMLibrary.PauseLibrary();
    }

    // Update is called once per frame
    void Update()
    {
        FSMLibrary.PriorityUpdate();
        FSMLibrary.Update();
    }

    void LateUpdate()
    {
        FSMLibrary.LateUpdate();
    }
}
