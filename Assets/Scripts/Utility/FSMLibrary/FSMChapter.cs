using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace FSMLibraryNS
{
    public class FSMChapter 
    {
	    // exceptions
	    public class ChapterAlreadyRunningException : System.ApplicationException
	    {
                public ChapterAlreadyRunningException() {}
                public ChapterAlreadyRunningException(string message) {}
                public ChapterAlreadyRunningException(string message, System.Exception inner) {}
	    }
	    public class ChapterNotRunningException : System.ApplicationException
	    {
                public ChapterNotRunningException() {}
                public ChapterNotRunningException(string message) {}
                public ChapterNotRunningException(string message, System.Exception inner) {}
	    }
	    public class StartingChapterNotSetException : System.ApplicationException
	    {
                public StartingChapterNotSetException() {}
                public StartingChapterNotSetException(string message) {}
                public StartingChapterNotSetException(string message, System.Exception inner) {}
	    }
	
	
	    // variables
	    private Dictionary<Type, IState> _States;
	    private Stack<Type> _StateStack;
	    private Type _NewPush;
	    private Object[] _PassthroughObjects;
	    private int _PopCount;
	    private int _ID;	
	    private RunningState _RunningState;

	    public int getID() { return _ID; }
	    public void setID(int _ID) { this._ID = _ID; }

	    // Private methods
        private void ChapterCreate(int nID)
	    {
		    _States = new Dictionary<Type, IState>();
            _StateStack = new Stack<Type>();
		    _NewPush = null;
		    _PopCount = 0;
		    _ID = nID;
		    _RunningState = RunningState.Suspended;
	    }

	    private void PushState_Internal(Type stateType)
	    {
		    _StateStack.Push(stateType);
		    if(_RunningState == RunningState.Running)
		    {
			    // pause current top
			    Type curTop = PeekNext();
			    if(curTop != null)
			    {
				    _States[curTop].OnPause();
			    }

                // pass and clear the passthrough
                _States[stateType].RetrievePassthrough(_PassthroughObjects);
                _PassthroughObjects = null;

			    _States[stateType].OnEnter();
			    _States[stateType].OnResume();
			
		    }
	    }

	    private void PopState_Internal(bool bExit)
	    {
		    Type top = _StateStack.Pop();
		
		    // only exit when running
		    if(_RunningState == RunningState.Running)
		    {
			    _States[top].OnPause();
			    _States[top].OnExit();
			
			    // resume new top
			    if(Peek() != null)
			    {
				    _States[Peek()].OnResume();
			    }
		    }
	    }
	
	    private void FlushStates()
	    {
		    // flush all states, exiting only on the top
		    bool bFirst = true;
		    while(_StateStack.Count > 0)
		    {
			    PopState_Internal(bFirst);
			    bFirst = false;
		    }
	    }

	    private IState CreateState(Type stateType)
	    {
			ConstructorInfo newConstructor = stateType.GetConstructor(new Type[] {});
			IState ret = (IState)newConstructor.Invoke(new object[] {});

		    return ret;
	    }

        // public processing functions
        public void StartChapter()
	    {
		    // ensure we have a start
		    if(_NewPush == null)
		    {
			    throw new StartingChapterNotSetException("Default state not set for chapter " + _ID);
		    }
		
		    if(_RunningState == RunningState.Running)
		    {
			    throw new ChapterAlreadyRunningException("Chapter is already running!");
		    }
		
		    // create all states
	        foreach (IState state in _States.Values) 
	        {
	    	    state.OnCreate();
	        }
		
		    _RunningState = RunningState.Running;
	    }

        public void StopChapter()
	    {
		    if(_RunningState == RunningState.Suspended)
		    {
			    throw new ChapterNotRunningException("Chapter isnt running!");
		    }
		    // exit all current states
		    FlushStates();
		
		    // destroy all states
	        foreach (IState state in _States.Values) 
	        {
	    	    state.OnDestroy();
	        }
		
	        _RunningState = RunningState.Suspended;
	    }

	    public void PauseChapter()
        {
		    if(_RunningState == RunningState.Suspended)
		    {
			    throw new ChapterNotRunningException("Chapter isnt running!");
		    }
		    // pause all states
	        foreach (IState state in _States.Values)
	        {
	    	    state.OnPause();
	        }
	        _RunningState = RunningState.Paused;
        }
	
	    public void ResumeChapter()
        {
		    if(_RunningState == RunningState.Suspended)
		    {
                throw new ChapterNotRunningException("Chapter isnt running or paused!");
		    }
		    // resume all states
	        foreach (IState state in _States.Values)
	        {
	    	    state.OnResume();
	        }
		    _RunningState = RunningState.Running;
        }

        public void PriorityUpdate()
        {
            // remove all pops 
            bool bFirst = true;
            while (_PopCount > 0)
            {
                PopState_Internal(bFirst);
                _PopCount--;
                bFirst = false;
            }

		    // push the new
		    if(_NewPush != null)
		    {
                // doing this in case we push a new in enter/resume
                Type toPush = _NewPush;
                _NewPush = null;
                PushState_Internal(toPush);
		    }
		
		    Type topState = Peek();
		    if(topState != null)
		    {
			    _States[topState].PriorityUpdate();		
		    }
	    }

        public void Update()
	    {
		    Type topState = Peek();
		    if(topState != null)
		    {
			    _States[topState].Update();		
		    }
	    }

        public void LateUpdate()
	    {		
		    Type topState = Peek();
		    if(topState != null)
		    {
			    _States[topState].LateUpdate();		
		    }
		
	    }
	
	    private Object Invoke(Type State, MethodInfo method, params object[] parameters)
	    {
		    Object ret = method.Invoke(_States[State], parameters);

		    return ret;
	    }

        public Object Call(String szMethod, params object[] parameters)
	    {		
		    Type topState = Peek();
		    if(topState != null)
		    {
			    // get type array
			    Type[] classTypes = new Type[parameters.Length];
			    int nCount = 0;
			    foreach(object obj in parameters)
			    {
				    classTypes[nCount++] = obj.GetType();
			    }
			
			    // get the method
			    MethodInfo method = topState.GetMethod(szMethod, classTypes.Length > 0 ? classTypes : null);
			 
			     // invoke it
			    return Invoke(topState, method, parameters);
		    }
		    return null;
	    }

        public Object[] CallQueue(String szMethod, params object[] parameters)
	    {		
		    if(_StateStack.Count > 0)
		    {
			    // get type array
			    Type[] classTypes = new Type[parameters.Length];
			    int nCount = 0;
			    foreach(object obj in parameters)
			    {
				    classTypes[nCount++] = obj.GetType();
			    }
			
			    // get the method
			    MethodInfo method = Peek().GetMethod(szMethod, classTypes);
	
			     // invoke it on all objects
			    nCount = 0;
			    Object[] ret = new Object[_StateStack.Count];
			    foreach(Type topState in _StateStack)
			    {
				    ret[nCount] = Invoke(topState, method, parameters);
			    }
			    return ret;
		    }
		    return null;
	    }
	
	    // public methods
        public FSMChapter(String szID)
	    {
		    ChapterCreate(szID.GetHashCode());
	    }

        public FSMChapter(int nID)
	    {
		    ChapterCreate(nID);
	    }
	
	    public void AddState(Type stateType)
	    {
		    // ensure we don't add while running
		    if(_RunningState == RunningState.Running)
		    {
			    throw new ChapterAlreadyRunningException("Chapter \"" + _ID + "\" is already running.  Use PushState() or ChangeState() instead.");
		    }
		    _States.Add(stateType, CreateState(stateType));
	    }
	
	    public void RemoveState(Type stateType)
	    {
		    // ensure we don't remove while running
		    if(_RunningState == RunningState.Running)
		    {
			    throw new ChapterAlreadyRunningException("Chapter \"" + _ID + "\" is already running.  Use PushState() or ChangeState() instead.");
		    }
		    _States.Remove(stateType);
	    }
	
	    public void SetDefaultState(Type stateType)
	    {
		    // ensure we don't set while running
		    if(_RunningState == RunningState.Running)
		    {
			    throw new ChapterAlreadyRunningException("Chapter \"" + _ID + "\" is already running.  Use PushState() or ChangeState() instead.");
		    }
		    _NewPush = stateType;
	    }

	    public Type Peek()
	    {
            if (_StateStack.Count == 0)
                return null;

		    Type ret = _StateStack.Peek();
		    return ret;
	    }
	
	    public Type PeekNext()
	    {
		    int nCount = 0;
		    foreach(Type ret in _StateStack)
		    {
			    if(nCount == 1)
			    {
				    return ret;
			    }
			    nCount++;
		    }
		    return null;
	    }
	
	    // TP
	    public void PushState(Type stateType, params object[] passThrough)
	    {
		    _NewPush = stateType;
		    _PassthroughObjects = passThrough;
	    }
	
	    public void PopState()
	    {
		    _PopCount++;
	    }
	
	    public void ChangeState(Type stateType, params object[] passThrough)
	    {
		    _PopCount = _StateStack.Count;
		    _NewPush = stateType;
		    _PassthroughObjects = passThrough;
	    }

        public void SetPassthrough(params object[] passThrough)
        {
		    _PassthroughObjects = passThrough;
        }
    }
}