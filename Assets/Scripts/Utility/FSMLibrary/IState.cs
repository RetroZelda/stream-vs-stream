
namespace FSMLibraryNS
{
    // TODO: make abstract so the functions aren't required
    public interface IState 
    {
	    void OnCreate();
	    void OnEnter();
	    void PriorityUpdate();
	    void Update();
	    void LateUpdate();
	    void OnExit();
	    void OnDestroy();
	    void OnPause();
	    void OnResume();
	    void RetrievePassthrough(params object[] _PassthroughObjects);
    }
}