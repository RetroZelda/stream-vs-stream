using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

namespace FSMLibraryNS
{
	// data types
	public enum RunningState {Suspended, Paused, Running}

    public class FSMLibrary 
    {	
	    // exceptions
	    public class StateExistsException : System.ApplicationException
	    {
            public StateExistsException() {}
            public StateExistsException(string message) {}
            public StateExistsException(string message, System.Exception inner) {}
	    }
	    public class FSMLibraryAlreadyRunningException : System.ApplicationException
	    {
            public FSMLibraryAlreadyRunningException() {}
            public FSMLibraryAlreadyRunningException(string message) {}
            public FSMLibraryAlreadyRunningException(string message, System.Exception inner) {}
	    }
	    public class FSMLibraryNotRunningException : System.ApplicationException
	    {
            public FSMLibraryNotRunningException() {}
            public FSMLibraryNotRunningException(string message) {}
            public FSMLibraryNotRunningException(string message, System.Exception inner) {}
	    }
	
	    // data
	    private List<IFSMConstructor> _Constructors;
	    private Dictionary<int, FSMChapter> _FSMChapters;	
	    private RunningState _RunningState;

        private class ConstructorStartData
        {
            public Type ConstructorType;
            public Object[] Passthrough;
        }
        private List<ConstructorStartData> _ConstructorToStart;
        private List<Type> _ConstructorToStop;

	    // singleton
	    private static FSMLibrary Instance = new FSMLibrary();	
	
        private FSMLibrary()
	    {
		    _Constructors = new List<IFSMConstructor>();
		    _FSMChapters = new Dictionary<int, FSMChapter>();
		    _RunningState = RunningState.Suspended;
            _ConstructorToStart = new List<ConstructorStartData>();
            _ConstructorToStop = new List<Type>();
	    }
	    /////////////////////////////////////////////////////////
	
	    // private functions
	    private IFSMConstructor CreateConstructor(Type constructorType)
	    {
		    IFSMConstructor ret = null;

            // No parameters in the constructor
            ConstructorInfo constructor = constructorType.GetConstructor(new Type[] {});
			ret = (IFSMConstructor)constructor.Invoke(new object[] {});

		    return ret;
	    }
	
	    private void StartNewChapter_Internal(Type constructorType, params Object[] passThrough)
	    {
            ConstructorStartData data = new ConstructorStartData();
            data.ConstructorType = constructorType;
            data.Passthrough = passThrough;
            _ConstructorToStart.Add(data);
	    }
	
	    private void StopChapter_Internal(Type constructorType)
	    {
            _ConstructorToStop.Add(constructorType);
	    }

	    private void AddConstructor_Internal(Type constructorType) 
	    {
		    if(_RunningState == RunningState.Running)
		    {
			    throw new FSMLibraryAlreadyRunningException("FSM Library is already running!");
		    }
		    if(_RunningState == RunningState.Paused)
		    {
			    throw new FSMLibraryNotRunningException("FSM Library is paused!");
		    }
		    IFSMConstructor constructor = CreateConstructor(constructorType);  
		    if(_FSMChapters.ContainsKey(constructor.ChapterHash))
		    {
			    throw new StateExistsException("Constructor \"" + constructor.Chapter + "\" already exists!");
		    }
		
		    if(!_FSMChapters.ContainsKey(constructor.ChapterHash))
		    {
			    AddChapter(constructor.ChapterHash);
		    }
		    _Constructors.Add(constructor);
	    }
	
	    private void AddChapter(int nChapterHash)
	    {
		    _FSMChapters.Add(nChapterHash, new FSMChapter(nChapterHash));
	    }

	    private void Build_Internal()
	    {
		    for(int nConstructorIndex = 0; nConstructorIndex < _Constructors.Count; ++nConstructorIndex)
		    {
			    _Constructors[nConstructorIndex].Build();
		    }
		    _Constructors.Clear();
	    }
	
	    private FSMChapter GetChapter(int nID)
	    {
		    if(!_FSMChapters.ContainsKey(nID))
		    {
			    AddChapter(nID);
		    }
		    return _FSMChapters[nID];
	    }
	
	    // private processing functions
	    private void StartLibrary_Internal()
	    {
		    if(_RunningState == RunningState.Running)
		    {
			    throw new FSMLibraryAlreadyRunningException("FSM Library is already running!");
		    }
		    if(_RunningState == RunningState.Paused)
		    { 
			    throw new FSMLibraryNotRunningException("FSM Library is paused!");
		    }
		
		    foreach(FSMChapter chapter in _FSMChapters.Values)
		    {
	            chapter.StartChapter();
		    }

		    _RunningState = RunningState.Running;
	    }
	
	    private void PriorityUpdate_Internal() 
	    {
		    if(_RunningState == RunningState.Suspended)
		    {
			    throw new FSMLibraryNotRunningException("FSM Library isnt running!");
		    }
		    if(_RunningState == RunningState.Paused)
		    {
			    throw new FSMLibraryNotRunningException("FSM Library is paused!");
		    }

            // remove all stopped FSMs
            if (_ConstructorToStop.Count > 0)
            {
                foreach (Type t in _ConstructorToStop)
                {
                    IFSMConstructor constructor = CreateConstructor(t);

                    GetChapter(constructor.ChapterHash).StopChapter();
                    constructor.Destroy();
                }
                _ConstructorToStop.Clear();
            }

            // process all added FSMs
            if (_ConstructorToStart.Count > 0)
            {
                foreach (ConstructorStartData data in _ConstructorToStart)
                {
                    IFSMConstructor constructor = CreateConstructor(data.ConstructorType);
                    constructor.Build();

                    GetChapter(constructor.ChapterHash).SetPassthrough(data.Passthrough);
                    GetChapter(constructor.ChapterHash).StartChapter();
                }
                _ConstructorToStart.Clear();
            }
		
		    foreach(FSMChapter chapter in _FSMChapters.Values)
		    {
			    chapter.PriorityUpdate();
		    }
	    }
	
	    private void Update_Internal()
	    {
		    if(_RunningState == RunningState.Suspended)
		    {
			    throw new FSMLibraryNotRunningException("FSM Library isnt running!");
		    }
		    if(_RunningState == RunningState.Paused)
		    {
			    throw new FSMLibraryNotRunningException("FSM Library is paused!");
		    }
		
		    foreach(FSMChapter chapter in _FSMChapters.Values)
		    {
			    chapter.Update();
		    }
	    }
	
	    private void LateUpdate_Internal()
	    {
		    if(_RunningState == RunningState.Suspended)
		    {
			    throw new FSMLibraryNotRunningException("FSM Library isnt running!");
		    }
		    if(_RunningState == RunningState.Paused)
		    {
			    throw new FSMLibraryNotRunningException("FSM Library is paused!");
		    }
		
		    foreach(FSMChapter chapter in _FSMChapters.Values)
		    {
			    chapter.LateUpdate();
		    }
	    }
	
	    private void StopLibrary_Internal() 
	    {
		    if(_RunningState == RunningState.Suspended)
		    {
			    throw new FSMLibraryNotRunningException("FSM Library isnt running!");
		    }
		
		    foreach(FSMChapter chapter in _FSMChapters.Values)
		    {
	            chapter.StopChapter();
		    }
		
		    _RunningState = RunningState.Suspended;
	    }

	    private void ResumeLibrary_Internal()
        {
		    if(_RunningState == RunningState.Suspended)
		    {
			    throw new FSMLibraryNotRunningException("FSM Library isnt running!");
		    }
		    if(_RunningState != RunningState.Running)
		    {
			    foreach(FSMChapter chapter in _FSMChapters.Values)
			    {
	                chapter.ResumeChapter();
			    }
			    _RunningState = RunningState.Running;
		    }
        }
	
	    private void PauseLibrary_Internal()
        {
		    if(_RunningState == RunningState.Suspended)
		    {
			    throw new FSMLibraryNotRunningException("FSM Library isnt running!");
		    }
		    if(_RunningState != RunningState.Paused)
		    {
			    foreach(FSMChapter chapter in _FSMChapters.Values)
			    {
	                chapter.PauseChapter();
			    }
			    _RunningState = RunningState.Paused;
		    }
        }
	
	    private Object Call_Internal(int nChapterHash, String szMethod, params object[] parameters)
	    {
		    if(_RunningState == RunningState.Suspended)
		    {
			    throw new FSMLibraryNotRunningException("FSM Library isnt running!");
		    }
		    if(_RunningState == RunningState.Paused)
		    {
			    throw new FSMLibraryNotRunningException("FSM Library is paused!");
		    }
		    return Chapter(nChapterHash).Call(szMethod, parameters);
	    }
	
	    private Object[] CallQueue_Internal(int nChapterHash, String szMethod, params object[] parameters)
	    {
		    if(_RunningState == RunningState.Suspended)
		    {
			    throw new FSMLibraryNotRunningException("FSM Library isnt running!");
		    }
		    if(_RunningState == RunningState.Paused)
		    {
			    throw new FSMLibraryNotRunningException("FSM Library is paused!");
		    }
		    return Chapter(nChapterHash).CallQueue(szMethod, parameters);
	    }

	    // public static bridge functions
	    public static FSMChapter Chapter(int nID)
	    {
		    return Instance.GetChapter(nID);
	    }
	
	    public static FSMChapter Chapter(String szID)
	    {
		    return Instance.GetChapter(szID.GetHashCode());
	    }
	
	    // public static functions
	    public static void StartNewChapter<T>(params object[] passThrough) where T : IFSMConstructor
	    {
            Type constructorType = typeof(T);
		    Instance.StartNewChapter_Internal(constructorType, passThrough);
	    }
	
	    public static void StopChapter<T>() where T : IFSMConstructor
	    {
            Type constructorType = typeof(T);
		    Instance.StopChapter_Internal(constructorType);
	    }
	
	    public static void AddConstructor<T>() where T : IFSMConstructor
	    {
            Type constructorType = typeof(T);
		    Instance.AddConstructor_Internal(constructorType);
	    }

        public static void AddConstructor(Type constructorType)
        {
            if (constructorType is IFSMConstructor)
                Instance.AddConstructor_Internal(constructorType);
            else
                throw new InvalidCastException("Submitted type is not of type IFSMConstructor");
        }

        public static void Build()
	    {
		    Instance.Build_Internal();
	    }
	
	    // public static bridge processing functions
	    public static void StartLibrary()
	    {
		    Instance.StartLibrary_Internal();
	    }
	
	    public static void PriorityUpdate()
	    {
		    Instance.PriorityUpdate_Internal();
	    }
	
	    public static void Update()
	    {
		    Instance.Update_Internal();
	    }
	
	    public static void LateUpdate() 
	    {
		    Instance.LateUpdate_Internal();
	    }
	
	    public static void StopLibrary() 
	    {
		    Instance.StopLibrary_Internal();
	    }

	    public static Object Call(int nChapterHash, String szMethod, params object[] parameters) 
	    {
		    return Instance.Call_Internal(nChapterHash, szMethod, parameters);
	    }

        public static Object[] CallQueue(int nChapterHash, String szMethod, params object[] parameters) 
	    {
		    return Instance.CallQueue_Internal(nChapterHash, szMethod, parameters);
	    }

	    public static void PauseLibrary() 
        {
		    Instance.PauseLibrary_Internal();
        }

	    public static void ResumeLibrary() 
        {
		    Instance.ResumeLibrary_Internal();
        }

    }
}