﻿using UnityEngine;
using System.Collections;

public class Global : MonoBehaviour
{
    [SerializeField]
    private PrefabLibrary _prefabLib;

    public static Global Instance { get; private set; }
    public static PrefabLibrary PrefabLib { get { return Instance._prefabLib; } }

    void Awake()
    {
	    if(Instance != null)
        {
            Debug.LogError("Global already defined!");
            enabled = false;
            return;
        }

        Instance = this;
    }
}
