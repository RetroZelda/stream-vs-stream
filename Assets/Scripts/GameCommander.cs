﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Twitch;

public delegate void OnChatMessageEvent(IChatBot sender, string szName, string szText);
public delegate void OnCommandEvent(string szName, string szText);
public enum StreamService { Twitch, Youtube, Facebook };

public class GameCommander : MonoBehaviour
{
    
    [SerializeField]
    private string[] _GameCommands; // the commands that this game is going to be requiring.
    public List<string> GameCommands { get; private set; }

    [SerializeField]
    private string[] _SystemCommands; // the commands that players use to modify their experience
    public List<string> SystemCommands { get; private set; }

    [SerializeField]
    private CommandListener[] _StreamUsers; // to set the stream accounts to be used in the inspector

    [SerializeField]
    private bool _bInitOnStart = false; // for inspector

    [HideInInspector]
    public static bool INITONSTART = true; // for code

    /// <summary>
    /// A Link between a stream service, and in game player
    /// </summary>
    private Dictionary<IChatBot, CommandListener> _RegisteredStreams = new Dictionary<IChatBot, CommandListener>();

    /// <summary>
    /// A link between a hashed username and the player they are registered under
    /// </summary>
    private Dictionary<int, PlayerSpawnInformation> _UserTeamRegistration = new Dictionary<int, PlayerSpawnInformation>();

    private PlayerSpawnInformation[] _SpawnInfo;
    public PlayerSpawnInformation[] SpawnInfo { get { return _SpawnInfo; } private set { _SpawnInfo = value; } }     

    void Start()
    {
        if(_bInitOnStart && INITONSTART)
        {
            Debug.LogWarning("Initializing the GameCommander on Start!  This should Only be done for Debugging purposes.");
            Initialize();
        }
    }

    public void Initialize ()
    {
        List<PlayerSpawnInformation> SpawnList = new List<PlayerSpawnInformation>();
        foreach(CommandListener user in _StreamUsers)
        {
            switch(user.Serivice)
            {
                case StreamService.Twitch:

                    // create the twitch bot
                    TwitchChat twitch = gameObject.AddComponent<TwitchChat>();
                    twitch.TwitchUser = user.Username;
                    twitch.TwitchOAuthToken = user.Password;
                    twitch.TwitchChannel = user.Channel;
                    twitch.OnChatMessage += ParseChat;
                    twitch.Connect();
                    _RegisteredStreams.Add(twitch, user);

                    // set our playerIDs
                    user.GenerateTeamIDs(twitch, 1, LogToStream);
                    SpawnList.AddRange(user.TeamIDs);

                    break;
                case StreamService.Youtube:
                    break;
                case StreamService.Facebook:
                    break;
            }

        }

        // get our final list of spawn info
        SpawnInfo = SpawnList.ToArray();

        // save all game commands into a list
        GameCommands = new List<string>(_GameCommands.Length);
        foreach (string command in _GameCommands)
        {
            GameCommands.Add(command.ToLower());
        }

        // save all system commands into a list
        SystemCommands = new List<string>(_SystemCommands.Length);
        foreach (string command in _SystemCommands)
        {
            SystemCommands.Add(command.ToLower());
        }
    }

    /// <summary>
    /// Split a message up into command line arguements
    /// </summary>
    /// <param name="szMessage">The Message to Parse</param>
    /// <param name="commandArgs">The output arguments. [0] is always the command, [n+1] are always the arguments</param>
    private void ParseMessageForSystemCommands(string szMessage, out string[] commandArgs)
    {
        Debug.Log(string.Format("Parsing message for commands: \"{0}\".", szMessage));
        commandArgs = szMessage.ToLower().Split(' ');
    }
    	
    /// <summary>
    /// A callback when a stream client has submit a message
    /// </summary>
    /// <param name="sender">The stream client that hte command was recieved from</param>
    /// <param name="szUsername">the username of the user who sent the command</param>
    /// <param name="szMessage">the message the user has submit</param>
    private void ParseChat(IChatBot sender, string szUsername, string szMessage)
    {
        // check if the user sent in a command from our list
        if (GameCommands.Contains(szMessage.ToLower()))
        {
            // ensure the user is registered
            int nUserHash = szUsername.GetHashCode();
            if (_UserTeamRegistration.ContainsKey(nUserHash))
            {
                PlayerSpawnInformation team = _UserTeamRegistration[nUserHash];

                Debug.Log(string.Format("[COMMAND][{0}][{1}][{2}]", szUsername, team.TeamID, szMessage.ToLower()));
                team.BroadcastCommand(szUsername, szMessage.ToLower());
            }
            else
            {
                LogToStream(sender, szUsername, "You haven't chosen a team yet!  Use the \"JoinTeam [TeamID]\" command to pick!");
            }
            sender.PurgeMessage(szUsername, 1);
        }
        else
        {
            // check for a system command
            string[] commandArgs;
            ParseMessageForSystemCommands(szMessage, out commandArgs);
            if(SystemCommands.Contains(commandArgs[0]))
            {
                HandleSystemCommand(sender, szUsername, commandArgs);
                sender.PurgeMessage(szUsername, 1);
            }
        }
    }

    private void HandleSystemCommand(IChatBot sender, string szUsername, string[] commandArgs)
    {
        switch (commandArgs[0])
        {
            case "jointeam":
                // ensure there are enough command args
                if(commandArgs.Length >= 2)
                {
                    // TODO: User command arg[1] to get the CommandListener assigned to that player and handle invalid input
                    if(!SetUserRegistration(sender, szUsername, commandArgs[1]))
                    {
                        LogToStream(sender, szUsername, string.Format("Failed to register {0} to Team {1}. Team does not exist!", szUsername, commandArgs[1]));
                    }
                    else
                    {
                        LogToStream(sender, "", string.Format("Registering {0} to Team {1} Successful", szUsername, commandArgs[1]));
                    }
                }
                else
                {
                    LogToStream(sender, szUsername, "You forgot to determine which team to join!  Use the \"JoinTeam [TeamID]\" command to pick!");
                }
                break;
            default:
                Debug.Log(string.Format("Unhandled Command \"{0}\" with {1} arguments.", commandArgs[0], commandArgs.Length - 1));
                break;
        }
    }

    /// <summary>
    /// Registers a user with their preferred stream player
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="nUserHash"></param>
    private bool SetUserRegistration(IChatBot sender, string szUsername, string szTeamID)
    {
        PlayerSpawnInformation teamToRegister = null;
        foreach(CommandListener usr in _StreamUsers)
        {
            foreach(PlayerSpawnInformation Team in usr.TeamIDs)
            {
                if(Team.TeamID == szTeamID)
                {
                    teamToRegister = Team;
                    break;
                }
            }

            if (teamToRegister != null)
                break;
        }

        // team not found 
        if (teamToRegister == null)
            return false;

        int nUserHash = szUsername.GetHashCode();
        if (_UserTeamRegistration.ContainsKey(nUserHash))
        {
            _UserTeamRegistration[nUserHash] = teamToRegister;
        }
        else
        {
            _UserTeamRegistration.Add(nUserHash, teamToRegister);
        }

        return true;
    }

    /// <summary>
    /// Log a message to the stream's chat
    /// </summary>
    /// <param name="stream">The stream to log to</param>
    /// <param name="szUser">The user to PM.  Empty string or null for everyone</param>
    /// <param name="szMessage">the message to send.</param>
    private void LogToStream(IChatBot stream, string szUser, string szMessage)
    {
        if(_RegisteredStreams[stream].SendMessagesToClient)
        {
            stream.SendMessage(szUser, szMessage);
        }
    }
}
