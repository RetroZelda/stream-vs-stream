﻿using System.IO;
using UnityEngine;
using System.Threading;
using System.Net.Sockets;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Twitch
{
    /// <summary>
    /// A Twitch-Specific IRC client
    /// </summary>
    public class TwitchChat : MonoBehaviour, IChatBot
    {
        private readonly string PING_RESPONSE = "PONG :tmi.twitch.tv";

        [SerializeField]
        private string _IRCAddress = "irc.chat.twitch.tv";

        [SerializeField]
        private string _IRCResponse = "tmi.twitch.tv";

        [SerializeField]
        private int _IRCPort = 6667;

        [SerializeField]
        private int _IRCSSLPort = 443;

        [SerializeField]
        private bool _UseSSL = false;

        [SerializeField]
        private string _twitchUser = "efolckemer";
        public string TwitchUser { get { return _twitchUser; } set { _twitchUser = value; } }

        /// <summary>
        /// Oauth Token is used as IRC password.
        /// The token must have the prefix of oauth:. For example, if you have the token abcd, you send oauth:abcd.
        /// </summary>
        [SerializeField]
        private string _twitchOauthToken = "9yexmvmzdcbk56q4o93cj451vrikcm";
        public string TwitchOAuthToken
        {
            get { return "oauth:" + _twitchOauthToken; }
            set
            {
                _twitchOauthToken = value;
            }
        }

        [SerializeField]
        private string _MessageHeader = "GameBot[";

        [SerializeField]
        private string _MessageFooter = "]";

        [SerializeField]
        private bool _isInvisible = true;
        
        [SerializeField]
        private string _Channel = "";
        public string TwitchChannel
        {
            get { return _Channel; }
            set
            {
                if (IsConnected)
                    Write("PART {0}", _Channel);

                    _Channel = value;
                    if (_Channel[0] != '#')
                        _Channel = "#" + _Channel;

                if (IsConnected)
                    Write("JOIN {0}", _Channel);
            }
        }

        public bool IsConnected { get { return IrcConnection != null ? IrcConnection.Connected : false; } }

        private TcpClient IrcConnection { get; set; }
        private NetworkStream IrcStream { get; set; }
        private StreamWriter IrcWriter { get; set; }
        private StreamReader IrcReader { get; set; }
        private Thread ReadThread { get; set; }
        private LinkedList<string[]> _CommandQueue = new LinkedList<string[]>();

        private OnChatMessageEvent _OnChatMessage;
        public OnChatMessageEvent OnChatMessage { get { return _OnChatMessage; } set { _OnChatMessage = value; } }

        /// <summary>
        /// Send a message to the chat.
        /// </summary>
        /// <param name="szUser">The user to whisper. Put an empty string or null to braodcast to everyone</param>
        /// <param name="szMessage"></param>
        public void SendMessage(string szUser, string szMessage)
        {
            string szFinalMessage = _MessageHeader + szMessage + _MessageFooter;
            // Handle sending private messages
            if (string.IsNullOrEmpty(szUser))
            {
                Write(":{0}!{0}@{0}.tmi.twitch.tv PRIVMSG {1} :{2}", TwitchUser, TwitchChannel, szFinalMessage);
            }
            else
            {
                string szPrivateMessage = "/w " + szUser + " ";
                Write(":{0}!{0}@{0}.tmi.twitch.tv PRIVMSG {1} :/w {2} {3}", TwitchUser, TwitchChannel, szUser, szFinalMessage);
            }
        }

        public void PurgeMessage(string szUser, int nTimeout)
        {
            // Write(":{0}!{0}@{0}.tmi.twitch.tv PRIVMSG {1} .timeout {2} {3}", TwitchUser, TwitchChannel, szUser, nTimeout);
        }

        public void Connect()
        {
            StartCoroutine(ConnectRoutine());
        }

        public void Disconnect()
        {
            if(IsConnected)
                Write("PART {0}", TwitchChannel);

            ReadThread.Abort();
            IrcWriter.Close();
            IrcReader.Close();
            IrcConnection.Close();
        }
        
        void Start()
        {
        }

        void Awake()
        {
            ReadThread = new Thread(new ThreadStart(ThreadMain));
        }

        void OnDisable()
        {
            Disconnect();
        }

        void Update()
        {
            lock (_CommandQueue)
            {
                while(_CommandQueue.Count > 0)
                {
                    ParseCommand(_CommandQueue.First.Value);
                    _CommandQueue.RemoveFirst();
                }
            }
        }

        private IEnumerator ConnectRoutine()
        {
            // connect
            IrcConnection = new TcpClient(_IRCAddress, _IRCPort);
            IrcStream = IrcConnection.GetStream();
            IrcReader = new StreamReader(IrcStream);
            IrcWriter = new StreamWriter(IrcStream);
            
            ReadThread.Start();

            // authenticate
            Write("PASS {0}", TwitchOAuthToken);
            Write("NICK {0}", _twitchUser);
            Write("JOIN {0}", TwitchChannel);
            yield return null;
        }

        private void ParseCommand(string[] commandParts)
        {
            if (commandParts[0] == _IRCResponse)
            {
                // Server message
                switch (commandParts[1])
                {
                    case "332": // Topic
                        break;
                    case "333": // Owner
                        break;
                    case "353": // Names List
                        break;
                    case "366": // End Names List
                        break;
                    case "372": // MOTD
                        break;
                    case "376": // END MOTD
                        break;
                    default: // Unhandled
                        break;
                }
            }
            else if (commandParts[0] == "PING")
            {
                // Server PING, send PONG back
                Write(PING_RESPONSE);
            }
            else
            {
                // Normal message
                string commandAction = commandParts[1];
                switch (commandAction)
                {
                    case "PRIVMSG":
                        if (OnChatMessage != null)
                        {
                            // rebuild the full chat message, reinserting spaces
                            string szFullChatMessage = commandParts[3].Substring(1);
                            for(int nIndex = 4; nIndex < commandParts.Length; ++nIndex)
                            {
                                szFullChatMessage += " " + commandParts[nIndex];
                            }

                            OnChatMessage(this, ParseUsername(commandParts[0]), szFullChatMessage);
                        }
                        break;
                    case "JOIN": break;
                    case "PART": break;
                    case "MODE": break;
                    case "NICK": break;
                    case "KICK": break;
                    case "QUIT": break;
                    default: // Unhandled
                        break;
                }
            }
        }

        private static string ParseUsername(string szFullUser)
        {
            int nStart = szFullUser.IndexOf('!') + 1;
            int nEnd = szFullUser.IndexOf('@');
            return szFullUser.Substring(nStart, nEnd - nStart) ;
        }
        
        #region ReadThread

        public void Write(string szText, params object[] arg)
        {
            lock(IrcWriter)
            {
                Debug.Log("SEND:  " + string.Format(szText, arg));
                IrcWriter.WriteLine(szText, arg);
                IrcWriter.Flush();
            }
        }

        public bool Read(out string szRead)
        {
            lock(IrcReader)
            {
                szRead = IrcReader.ReadLine();
            }
            return !string.IsNullOrEmpty(szRead);
        }

        private void ThreadMain()
        {
            if (IsConnected)
            {
                // read in all commands
                string recievedCommand = "";
                while (Read(out recievedCommand))
                {
                    Debug.Log("RECV: " + recievedCommand);

                    // strip the colon
                    string[] commandParts = recievedCommand.Split(' ');
                    if (commandParts[0].Substring(0, 1) == ":")
                    {
                        commandParts[0] = commandParts[0].Remove(0, 1);
                    }

                    lock(_CommandQueue)
                    {
                        _CommandQueue.AddLast(commandParts);
                    }
                }
            }
        }
        #endregion
    }
}