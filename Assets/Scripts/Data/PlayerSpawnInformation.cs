﻿using UnityEngine;
using System.Collections;

public class PlayerSpawnInformation
{
    private CommandListener _CommandListener;
    private IChatBot _StreamBot;
    private string _TeamID;
    
    public CommandListener CommandListener { get { return _CommandListener; } }
    public IChatBot StreamBot { get { return _StreamBot; } }
    public string TeamID { get { return _TeamID; } }

    /// <summary>
    /// Callback when this team receives a command
    /// </summary>
    public OnCommandEvent OnGameCommand;
    public void BroadcastCommand(string szUsername, string szCommand)
    {
        if (OnGameCommand != null)
        {
            OnGameCommand(szUsername, szCommand);
        }
    }

    public PlayerSpawnInformation(CommandListener cmd, IChatBot bot, string szTeamID)
    {
        _CommandListener = cmd;
        _StreamBot = bot;
        _TeamID = szTeamID;
    }
}
