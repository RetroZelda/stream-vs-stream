﻿using UnityEngine;
using System.Collections;
using System;

[System.Serializable]
public class CommandListener
{
    [SerializeField]
    private StreamService _Serivice;
    public StreamService Serivice { get { return _Serivice; } }

    [SerializeField]
    private string _Username;
    public string Username { get { return _Username; } }

    [SerializeField]
    private string _Password;
    public string Password { get { return _Password; } }

    [SerializeField]
    private string _Channel;
    public string Channel { get { return _Channel; } }

    [SerializeField]
    private bool _SendMessagesToClient = false;
    public bool SendMessagesToClient { get { return _SendMessagesToClient; } }

    [SerializeField]
    private int _NumOfTeams; // number of players this stream is controlling
    public int NumOfTeams { get { return _NumOfTeams; } }

    [SerializeField]
    private string _TeamPrefix = "p"; // prefix for each player on this team
    public string TeamPrefix { get { return _TeamPrefix; } }

    public PlayerSpawnInformation[] TeamIDs { get; set; }


    public void GenerateTeamIDs(IChatBot owningStream, int nStartIDNumber = 1, Action<IChatBot, string, string> logFunc = null)
    {
        TeamIDs = new PlayerSpawnInformation[_NumOfTeams];
        int nIdNum = nStartIDNumber;
        for (int nTeamCount = 0; nTeamCount < _NumOfTeams; ++nTeamCount)
        {
            TeamIDs[nTeamCount] = new PlayerSpawnInformation(this, owningStream, TeamPrefix + (nIdNum++).ToString());

            if(logFunc != null)
            {
                logFunc(owningStream, "", string.Format("Player Added: \"{0}\"", TeamIDs[nTeamCount].TeamID));
            }
        }
    }
}